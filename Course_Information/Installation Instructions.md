---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.1'
      jupytext_version: 1.2.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

<!-- #region -->
# Installation instructions

This page explains how to: 

1. Install python using Anaconda
2. Install `nbextensions`
3. Start a Jupyter Notebook server
4. Enable the "Table of Contents" Extension

## 1. Install python using Anaconda

First, download and install Anaconda:

https://www.anaconda.com/distribution/

Run the installer with the default options. 

**Warning:** Even if you already have Anaconda installed, we recommend that you download it again and re-install it to make sure you have the latest version of the packages. 

## 2. Install the notebook extensions

On **Windows**, open an "Anaconda prompt" (type "Anaconda prompt" in search in Windows 10, or look for it in the program menu):

<img src="anaconda_prompt_windows.png" width=50%></img>
          
On **MacOS**, open a "Teminal" app (use "Spotlight Search" and type "Terminal"): 

<img src="mac_terminal.png" width=40%></img>

At the command line of the terminal, run the following command by copy-pasting in the text and typing "enter":

`conda install -c conda-forge jupyter_contrib_nbextensions`

When it is done, you can close the terminal, or you can leave it open for the next step. 

## 3. Start a jupyter notebook server

From the "Anaconda prompt" (in windows) or from "Terminal" (on MacOS), run the following command:

`jupyter notebook`

This will start the notebook server and automatically open a tab in your web browser sending you to the (local) web address of the notebook server running on your computer, showing you the files in your home directory:

<img width=70% src="notebook_server.png"><img>

If you close your browser, you can always use this link to get back to your local notebook server:

http://localhost:8888/

You should leave the terminal running and minimize the window. 
 
## 4. Enable the "Table of Contents" notebook extension

The notebooks for the Python Practicum course use the "Table of contents" notebook extension to make enable section numbers and tables of contents. (The notebooks will work fine without them, but you will miss the table of contents button in the toolbar and you will miss the section numbering.)

To enable it, go to the notebook server home page:

http://localhost:8888/

Click on the "Nbextensions" tab:

<img src="nbextensions.png" width=50%></img> 

Make sure that this box is not clicked:

<img src="disable_box.png" width=50%></img>

<p> And enable the "Table of Contents" extension:

<img src="enable_toc.png" width=50%></img>

## Frequently asked questions

### My Jupyter server has crashed, link does not work

If the link http://localhost:8888 does not work, your server may have crashed (or may be running on a different port if you accidentally started muliple servers). 

 If this is the case, you should go back to the Anaconda prompt window it was running in and do the following:

1. Type `Ctrl-C`
2. Run the command `jupyter notebook stop`
3. Run the command `jupyter notebook start`

If you cannot find the Anaconda prompt window anymore, you can also open a new Anaconda prompt window and do steps 2 and 3 above. 


### My files are not in my home directory, how do I get to them? 

If your files are stored outside of your home directory, for example if they are on your D: drive in windows, you will have to first change directories in the Anaconda prompt to that folder before you run the command `jupyter notebook`. 

For example, let's say your files are on the D: drive in a folder `D:\Python\Practicum`. First you should stop the notebook server (see steps 1 and 2 of "Restarting a notebook server") and then you can use the following commands in the Anaconda prompt: 

* `d:`
* `cd Python`
* `jupyter notebook`

This will start a notebook server with the "root directory" as `D:\Python`, which will allow you to access all the files in subfolders of `D:\Python`. 

### Why is my web browser going to http://localhost:8889/ (instead of 8888)?  

This can happen when you (accidentally) open multiple notebook servers. You can list all the notebook servers that are running using the following command at the Anaconda prompt:

`jupyter notebook list`

You can then close any extra ones you don't want by using the `stop` command with their "port" number (the four numbers after "localhost"). For example, to stop one running on port 8889, use:

`jupyter notebook stop 8889`
<!-- #endregion -->
